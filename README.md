# 👋 Hola, soy Samuel Ramírez (@Samvel24) </br>
Acerca de mí
- 💻 Actualmente estoy utilizando Java y las diferentes tecnologías que acompañan a este lenguaje
- 🌱 Busco colaborar y crecer el área de Desarrollo de Software
- 💓 Me encanta ser voluntario de trabajo social y dar mis manos para ayudar a otras personas
- 🎓 Estudié Ingeniería en el Instituto Politécnico Nacional - ESIME Zacatenco, México 📚
- 📫 Cómo contactarme: </br>
  - [![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/samuelramirez24/)
  - [![Platzi](https://img.shields.io/badge/Platzi-98CA3F?style=for-the-badge&logo=platzi&logoColor=white)](https://platzi.com/p/samvel24/)
## Habilidades computacionales
### Desarrollo de software
- [![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white)]()
[![Spring Boot](https://img.shields.io/badge/spring_boot-%236DB33F.svg?style=for-the-badge&logo=springboot&logoColor=white)]()
[![Spring Framework](https://img.shields.io/badge/spring_framework-%23.svg?style=for-the-badge&logo=spring&logoColor=white)]()
- [![Apache Maven](https://img.shields.io/badge/Apache%20Maven-C71A36?style=for-the-badge&logo=Apache%20Maven&logoColor=white)]() 
[![PHP](https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white)]()
[![jQuery](https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white)]()
- [![HTML](https://img.shields.io/badge/HTML-239120?style=for-the-badge&logo=html5&logoColor=white)]()
[![CSS](https://img.shields.io/badge/CSS-239120?&style=for-the-badge&logo=css3&logoColor=white)]()
[![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=white)]()

### Versionado de software
- [![Git](https://img.shields.io/badge/GIT-E44C30?style=for-the-badge&logo=git&logoColor=white)]()
[![Gitlab](https://img.shields.io/badge/gitlab-FCC624.svg?style=for-the-badge&logo=gitlab&logoColor=white)]()

### Bases de datos
- [![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)]()
[![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)]()

### Sistemas operativos
- [![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=black)]()
[![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)]()

### Ciencias de la computación
- [![Programación estructurada](https://img.shields.io/badge/Programación_estructurada-blue?style=for-the-badge)]()
[![Programación orientada a objetos](https://img.shields.io/badge/Programación_orientada_a_objetos-orange?style=for-the-badge)]()
[![Estrucutras de datos](https://img.shields.io/badge/Estructuras_de_datos-green?style=for-the-badge)]()
- [![API RESTful](https://img.shields.io/badge/API_RESTful-green?style=for-the-badge)]()
[![Clean Architecture](https://img.shields.io/badge/Clean_Architecture-orange?style=for-the-badge)]()
[![Modelo Vista Controlador](https://img.shields.io/badge/Modelo_Vista_Controlador-purple?style=for-the-badge)]()
- [![Procesamiento digital de imágenes](https://img.shields.io/badge/Procesamiento_digital_de_imágenes-yellow?style=for-the-badge)]()
[![Visión por computador](https://img.shields.io/badge/Visión_por_computador-brown?style=for-the-badge)]()

## Mis diplomas relacionados a tecnologías Java y desarrollo de software

- [Curso de Introducción a Java SE](https://platzi.com/p/samvel24/curso/1631-java-basico/diploma/detalle/)  
Fecha de expedición: Abril de 2023
- [Curso de Java SE Orientado a Objetos](https://platzi.com/p/samvel24/curso/1629-java-oop/diploma/detalle/)  
Fecha de expedición: Mayo de 2023
- [Curso Avanzado de Java SE](https://platzi.com/p/samvel24/curso/1236-java-avanzado/diploma/detalle/)  
Fecha de expedición: Junio de 2023
- [Curso de Arquitecturas Limpias para Desarrollo de Software](https://platzi.com/p/samvel24/curso/7759-arquitecturas-limpias/diploma/detalle/)  
Fecha de expedición: Julio de 2023
- [Curso de Java Spring](https://platzi.com/p/samvel24/curso/1996-java-spring/diploma/detalle/)  
Fecha de expedición: Agosto de 2023

***

**This Gitlab profile is driven by science ⚙ ⚛  🚀 💻 🤖**
<!---
Samvel24/Samvel24 is a ✨ special ✨ repository because its `README.md` (this file) appears on your Gitlab profile.
--->
